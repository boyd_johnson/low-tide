
import re

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select


from low_tide.config import Config


BASE_URL = "https://www.tide-forecast.com"
COUNTRY = "United States"


class Scraper(object):

    def __init__(self, config_filename, outfile, writer_class):
        """
        Args:
            config_filename (str):
            outfile (str):
            writer_class (Class):
        """

        self._config = Config(config_filename)

        self._writer = writer_class(outfile, self._config.fieldnames)

    def run(self):
        for location in self._config.locations:
            options = webdriver.ChromeOptions()
            options.binary_location = '/usr/bin/google-chrome-stable'
            options.add_argument('--headless')
            options.add_argument('--start-maximized')
            options.add_argument('disable-infobars')
            options.add_argument('--disable-extensions')
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.add_argument('window-size={}x{}'.format(8, 8))
            driver = webdriver.Chrome(chrome_options=options)
            driver.get(BASE_URL)
            driver.switch_to.frame(0)
            try:
                country_select = WebDriverWait(driver, 10).until(
                    EC.presence_of_element_located((By.ID, "country_id"))
                )
            except:
                print(driver.page_source.encode())
                raise
            else:
                print(country_select.get_attribute("innerHTML").encode())
                Select(country_select).select_element_by_value("1").click()

            try:
                state_select = WebDriverWait(driver, 10).until(
                    EC.presence_of_element_located((By.ID, "region_id"))
                )
            except:
                print(driver.page_source.encode())
                raise
            else:
                Select(state_select).select_by_visible_text(location.state).click()
            try:
                location = WebDriverWait(driver, 10).until(
                    EC.presence_of_element_located((By.ID, "location_filename_part"))
                )
            except:
                print(driver.page_source.encode())
                raise
            else:
                Select(location).select_by_visible_text(location.coastal_location).click()

            print(driver.find_element_by_class("tide-events"))