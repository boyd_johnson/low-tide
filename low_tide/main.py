import sys

from low_tide.cli import parse_args
from low_tide.log_writer.csv_writer import CsvWriter
from low_tide.scraper import Scraper


def main(args=sys.argv[1:]):
    opts = parse_args(args)

    scraper = Scraper(opts.config, opts.outfile, CsvWriter)

    scraper.run()
