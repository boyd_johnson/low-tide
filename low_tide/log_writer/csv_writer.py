import csv


class CsvWriter(object):

    def __init__(self, filename, fieldnames):
        """
        Args:
            queue (asyncio.Queue): A queue of scraped data (dicts) that will
            be written to csv.
        """

        self._filename = filename
        self._fieldnames = fieldnames
        with open(self._filename, "w") as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=self._fieldnames)
            writer.writeheader()

    def write(self, data):
        """Write a dict of data to a csv file.

        Args:
            data (dict): The data to be written to the csv file.

        """

        with open(self._filename, "a") as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=self._fieldnames)
            writer.writerow(data)
