import yaml


FIELDNAMES_KEY = "fieldnames"
LOCATIONS_KEY = "locations"


class Config(object):

    def __init__(self, filename):
        data = None
        with open(filename, "r") as infile:
            data = yaml.safe_load(infile)

        if not data:
            raise ConfigException(
                "Unable to read yaml file: {}".format(filename))
        self.fieldnames = data[FIELDNAMES_KEY]
        self.locations = [
            Location(l[0], l[1])
            for l in data[LOCATIONS_KEY]
        ]


class ConfigException(Exception):
    pass


class Location(object):
    def __init__(self, state, coastal_location):
        self.state = state
        self.coastal_location = coastal_location
