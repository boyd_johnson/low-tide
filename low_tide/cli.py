import argparse


def parse_args(args):
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-c",
        "--config",
        required=True)

    parser.add_argument(
        "-o",
        "--outfile",
        required=True)

    return parser.parse_args(args)
