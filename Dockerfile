FROM ubuntu:xenial

RUN apt-get update && \
    apt-get install -yq \
        libnss3 \
        libglib2.0-0 \
        libgconf-2-4 \
        libfontconfig1 \
        python3 \
        python3-pip \
        unzip \
        wget

RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
    echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' > /etc/apt/sources.list.d/google-chrome.list && \
    apt-get update && \
    apt-get install -yq \
        google-chrome-stable

RUN pip3 install \
    selenium==3.13.0 \
    PyYAML==3.12

RUN wget https://chromedriver.storage.googleapis.com/2.40/chromedriver_linux64.zip && \
    unzip chromedriver_linux64.zip && mv chromedriver /usr/bin

WORKDIR /project/low-tide

ENV PATH=$PATH:/project/low-tide/bin
